@all @backend
Feature: Backend API

  Scenario: Register and Login - Success - API
    When I register a new user through API
    Then the current user has been registered successfully through API

  Scenario Outline: Register - Failure - Email - API
    Given a new user is generated randomly
    When I try to register a new user with email using API: <email>
    Then email registration with api fails with '<error>' message
    Examples:
      | email              | error                                      |
      |                    | Invalid Email.                             |
      | admin              | Invalid Email.                             |
      | admin@             | Invalid Email.                             |
      | admin@fmi.         | Invalid Email.                             |
      | admin@5342.licenta | Invalid Email.                             |
      | admin@fmi.licenta. | Invalid Email.                             |
      | admin@licenta.fmi  | Email has been registered to another user. |

  Scenario Outline: Register - Failure - Username - API
    Given a new user is generated randomly
    When I try to register a new user with username using API: <username>
    Then username registration with api fails with '<error>' message
    Examples:
      | username | error                |
      |          | Invalid username.    |
      | admin    | User already exists. |

  Scenario Outline: Register - Failure - Password - API
    Given a new user is generated randomly
    When I try to register a new user using API using password <password> and matching password <matchingPassword>
    Then password registration with api fails with '<error>' message
    Examples:
      | password                | matchingPassword        | error                                                                 |
      |                         |                         | Password or matching password cannot be empty.                        |
      | admin                   | admin1                  | Retyped password does not match provided password.                    |
      | thisisaverylongpassword | thisisaverylongpassword | Password should be less than 20 characters in length.                 |
      | short                   | short                   | Password should be more than 8 characters in length.                  |
      | Test                    | Test                    | Password should not be the same or contain the username.              |
      | 111                     | 111                     | Password should contain at least one upper case alphabetic character. |
      | 111                     | 111                     | Password should contain at least one lower case alphabetic character. |
      | 111                     | 111                     | Password should contain at least one special character.               |
      | aaaaA!                  | aaaaA!                  | Password should contain at least one numeric character.               |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one upper case alphabetic character. |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one lower case alphabetic character. |
      | !@$#@%$%$#!             | !@$#@%$%$#!             | Password should contain at least one numeric character.               |

  Scenario: Profile - Can edit own description - API
    Given I register a new user through API
    And I authenticate with API using the current user
    When I try to edit using API the user description with 'Test Description'
    Then the current user description has been edited using API successfully with 'Test Description'

  Scenario: Profile - Cannot edit other users description - API
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I try to edit using API the user description for user 2 with 'Test Description'
    Then I cannot edit through API other users description

  Scenario: Tweets - Post tweet - API
    Given I register a new user through API
    And I authenticate with API using the current user
    When I post a tweet using API with content 'Test Tweet'
    Then a tweet has been posted using API successfully with content 'Test Tweet'

  Scenario: Tweets - Cannot post tweet for other user - API
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I post a tweet using API for user 2 with content 'Test Tweet'
    Then a tweet has not been posted using API successfully for user 2 with content 'Test Tweet'

  Scenario: Tweet Likes - Like post
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I like a tweet using API with content 'Test Tweet'
    Then the tweet has been liked using API successfully

  Scenario: Tweet Likes - Like duplicate
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I like a tweet using API with content 'Test Tweet'
    And I like a tweet using API with content 'Test Tweet'
    Then an error response is returned for duplicate like

  Scenario: Tweet Likes - Tweet doesn't exist
    Given I register a new user through API
    And I authenticate with API using the current user
    When I like a tweet using API that does not exist
    Then an error response is returned for like failure

  Scenario: Tweet Likes - Unlike post
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I like a tweet using API with content 'Test Tweet'
    And I unlike a tweet using API with content 'Test Tweet'
    Then the tweet has been unliked using API successfully

  Scenario: Tweet Likes - Unlike duplicate
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I like a tweet using API with content 'Test Tweet'
    And I unlike a tweet using API with content 'Test Tweet'
    And I unlike a tweet using API with content 'Test Tweet'
    Then an error response is returned for duplicate unlike

  Scenario: Tweet Likes - Unlike duplicate
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I unlike a tweet using API with content 'Test Tweet'
    Then an error response is returned for unlike failure

  Scenario: Followers - Follow user - API
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I follow using API user 2
    Then user 1 is following user 2

  Scenario: Followers - Duplicate follow
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I follow using API user 2
    And I follow using API user 2
    Then an error response is returned for duplicate follow

  Scenario: Followers - Unfollow user - API
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I follow using API user 2
    And I unfollow using API user 2
    Then user 1 is not following user 2

  Scenario: Followers - Duplicate unfollow
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I follow using API user 2
    And I unfollow using API user 2
    And I unfollow using API user 2
    Then an error response is returned for duplicate unfollow

  Scenario: Followers - Unfollow failure
    Given I register through API 2 different users
    And I authenticate with API using user 1
    When I unfollow using API user 2
    Then an error response is returned for unfollow failure

  Scenario: Search - Users search
    Given I register a new user through API
    And I authenticate with API using the current user
    When I search users using API for 'Test'
    Then search results have been retrieved from API

  Scenario: Search - Users search - No results
    Given I register a new user through API
    And I authenticate with API using the current user
    When I search users using API for 'z'
    Then search results have not been retrieved from API

  Scenario: Search - Tweet search
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I search tweets using API for 'Test'
    Then search results have been retrieved from API

  Scenario: Search - Tweet search - No results
    Given I register a new user through API
    And I authenticate with API using the current user
    And I post a tweet using API with content 'Test Tweet'
    When I search tweets using API for 'z'
    Then search results have not been retrieved from API




