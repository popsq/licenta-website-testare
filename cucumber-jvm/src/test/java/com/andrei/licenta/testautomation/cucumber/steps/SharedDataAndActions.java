package com.andrei.licenta.testautomation.cucumber.steps;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.handlers.SearchRequestHandler;
import com.andrei.licenta.testautomation.api.handlers.TweetRequestHandler;
import com.andrei.licenta.testautomation.api.handlers.UserRequestHandler;
import com.andrei.licenta.testautomation.model.UserModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import com.andrei.licenta.testautomation.webdriver.WebDriverFactory;
import com.andrei.licenta.testautomation.webdriver.pageobjects.*;
import org.assertj.core.api.Fail;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SharedDataAndActions implements LogProvider {

    private WebDriverFactory webDriverFactory = new WebDriverFactory();
    private APIConnection apiConnection = new APIConnection();

    private Login loginPage;
    private Register registerPage;
    private Profile profilePage;
    private Search searchPage;
    private Header mainHeader;

    private UserRequestHandler userRequestHandler;
    private TweetRequestHandler tweetRequestHandler;
    private SearchRequestHandler searchRequestHandler;

    private UserModel currentUser = new UserModel();
    private List<UserModel> users = new ArrayList<>();

    public SharedDataAndActions() {}

    public void generateNewUser(){
        UserModel user = UserModel.generateRandomUser();
        users.add(user);
        currentUser = user;
    }

    public UserModel getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserModel currentUser) {
        this.currentUser = currentUser;
    }

    public List<UserModel> getUsers() {
        return users;
    }

    public void initWebDriver() {
        WebDriver webDriver = webDriverFactory.getDriver("chrome", WebDriverFactory.Browser.CHROME);
        initPageObjects(webDriver);
        webDriver.get("http://localhost:8080");
    }

    public void initAPI(){
        apiConnection.setBaseURI("http://localhost");
        apiConnection.setPort(8080);

        userRequestHandler = new UserRequestHandler(apiConnection);
        tweetRequestHandler = new TweetRequestHandler(apiConnection);
        searchRequestHandler = new SearchRequestHandler(apiConnection);
    }

    public void initPageObjects(WebDriver webDriver){
        loginPage = new Login(webDriver);
        registerPage = new Register(webDriver);
        profilePage = new Profile(webDriver);
        searchPage = new Search(webDriver);
        mainHeader = new Header(webDriver);
    }

    public void cleanup(){
        userRequestHandler.cleanup();

        userRequestHandler.setAuthenticationModel(null);
        tweetRequestHandler.setAuthenticationModel(null);
        searchRequestHandler.setAuthenticationModel(null);
        apiConnection.resetHeaders();

        currentUser = null;
        users.clear();
    }

    public Login getLoginPage() {
        return loginPage;
    }

    public Register getRegisterPage() {
        return registerPage;
    }

    public Profile getProfilePage() {
        return profilePage;
    }

    public Search getSearchPage() {
        return searchPage;
    }

    public Header getMainHeader() {
        return mainHeader;
    }

    public UserRequestHandler getUserRequestHandler() {
        return userRequestHandler;
    }

    public TweetRequestHandler getTweetRequestHandler() {
        return tweetRequestHandler;
    }

    public SearchRequestHandler getSearchRequestHandler() {
        return searchRequestHandler;
    }

    public void setCurrentUserBy(int id) {
        if (id < 1) {
            Fail.fail("User id must be greater or equal to 1.");
        }

        Optional<UserModel> userOptional = Optional.ofNullable(getUsers().get(id - 1));

        if (userOptional.isPresent()) {
            setCurrentUser(userOptional.get());
        } else {
            Fail.fail("No user has been registered with id {}", id);
        }
    }
}
