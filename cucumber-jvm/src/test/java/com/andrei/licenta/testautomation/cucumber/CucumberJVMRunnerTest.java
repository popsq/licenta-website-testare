package com.andrei.licenta.testautomation.cucumber;


import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        snippets = SnippetType.CAMELCASE,
        plugin = {"html:target/cucumber", "json:target/json/cucumber.json", "junit:target/junit/cucumber.xml"},
        features = "classpath:features",
        glue = "com.andrei.licenta.testautomation.cucumber.steps"
)
public class CucumberJVMRunnerTest {
}
