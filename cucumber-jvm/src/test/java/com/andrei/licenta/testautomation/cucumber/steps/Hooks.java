package com.andrei.licenta.testautomation.cucumber.steps;

import com.andrei.licenta.testautomation.utils.LogProvider;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;

public class Hooks implements LogProvider {

    private SharedDataAndActions sharedDataAndActions;

    public Hooks(SharedDataAndActions sharedDataAndActions) {
        this.sharedDataAndActions = sharedDataAndActions;
    }

    @Before
    public void beforeScenario(Scenario scenario) {
        logger().info("Starting test: [{}]", scenario.getName());
        if (scenario.getSourceTagNames().contains("@website")) {
            sharedDataAndActions.initWebDriver();
        }
        sharedDataAndActions.initAPI();
    }

    @After
    public void afterScenario(Scenario scenario) {
        if (scenario.isFailed()){
            logger().info("Test [{}] result: FAILED", scenario.getName());
        } else {
            logger().info("Test [{}] result: SUCCESS", scenario.getName());
        }

        logger().info("Starting cleanup");

        if (scenario.getSourceTagNames().contains("@website")) {
            sharedDataAndActions.getMainHeader().logout();
        }

        sharedDataAndActions.cleanup();
    }
}
