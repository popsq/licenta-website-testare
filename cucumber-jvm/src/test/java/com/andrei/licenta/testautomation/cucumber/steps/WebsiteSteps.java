package com.andrei.licenta.testautomation.cucumber.steps;

import com.andrei.licenta.testautomation.model.FollowerModel;
import com.andrei.licenta.testautomation.model.TweetModel;
import com.andrei.licenta.testautomation.model.UserModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.assertj.core.api.Fail;
import org.assertj.core.api.SoftAssertions;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;

public class WebsiteSteps implements LogProvider {

    private SharedDataAndActions sharedDataAndActions;

    private SoftAssertions softAssertions = new SoftAssertions();

    public WebsiteSteps(SharedDataAndActions sharedDataAndActions) {
        this.sharedDataAndActions = sharedDataAndActions;
    }

    @Given("^I register a new user$")
    public void iRegisterANewUser() throws Throwable {
        sharedDataAndActions.generateNewUser();

        sharedDataAndActions.getMainHeader().logout();
        sharedDataAndActions.getLoginPage().goToRegisterPage();
        sharedDataAndActions.getRegisterPage().doRegister(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @When("^I login with the current user$")
    public void iLoginWithTheCurrentUser() throws Throwable {
        sharedDataAndActions.getMainHeader().logout();
        sharedDataAndActions.getLoginPage().doLogin(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @Then("^the correct user profile is displayed$")
    public void theCorrectUserProfileIsDisplayed() throws Throwable {
        assertThat(sharedDataAndActions.getProfilePage().isCorrectProfile(
                sharedDataAndActions.getCurrentUser().getUsername()
        )).isTrue();
    }

    @When("^I try to login with (.*) and (.*)$")
    public void iTryToLoginWithEmailAndPassword(String email, String password) throws Throwable {
        sharedDataAndActions.getLoginPage().doLogin(email, password);
    }

    @Then("^HTML login validation fails$")
    public void htmlLoginValidationFails() throws Throwable {
        assertThat(sharedDataAndActions.getLoginPage().isValidationTriggered()).isTrue();
    }

    @Then("^login validation fails with '(.*)' message$")
    public void loginValidationFailsWithErrorMessage(String error) throws Throwable {
        assertThat(sharedDataAndActions.getLoginPage().getLoginErrorMessage()).isEqualToIgnoringCase(error);
    }

    @Given("^a new user is generated randomly$")
    public void aNewUserIsGeneratedRandomly() throws Throwable {
        sharedDataAndActions.generateNewUser();
    }

    @When("^I try to register a new user with email: (.*)$")
    public void iTryToRegisterANewUserWithEmail(String email) throws Throwable {
        sharedDataAndActions.getCurrentUser().setEmail(email);

        sharedDataAndActions.getLoginPage().goToRegisterPage();
        sharedDataAndActions.getRegisterPage().doRegister(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @When("^I try to register a new user with username: (.*)$")
    public void iTryToRegisterANewUserWithUsername(String username) throws Throwable {
        sharedDataAndActions.getCurrentUser().setUsername(username);

        sharedDataAndActions.getLoginPage().goToRegisterPage();
        sharedDataAndActions.getRegisterPage().doRegister(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @When("^I try to register a new user with password (.*) and matching password (.*)$")
    public void iTryToRegisterANewUserWithPassword(String password, String matchingPassword) throws Throwable {
        sharedDataAndActions.getCurrentUser().setPassword(password);
        sharedDataAndActions.getCurrentUser().setMatchingPassword(matchingPassword);

        sharedDataAndActions.getLoginPage().goToRegisterPage();
        sharedDataAndActions.getRegisterPage().doRegister(
                sharedDataAndActions.getCurrentUser()
        );
    }

    @Then("^(.*) registration fails with '(.*)' message$")
    public void typeRegistrationFailsWithErrorMessage(String type, String error) throws Throwable {
        switch (type.toLowerCase()) {
            case "email":
                if (Objects.equals("HTML", error)) {
                    assertThat(
                            sharedDataAndActions.getRegisterPage().isEmailValidationTriggered()
                    ).isTrue();
                } else {
                    assertThat(
                            sharedDataAndActions.getRegisterPage().getRegisterEmailErrorMessage()
                    ).contains(error);
                }
                break;
            case "password":
                assertThat(
                        sharedDataAndActions.getRegisterPage().getRegisterPasswordErrorMessage()
                ).contains(error);
                break;
            case "username":
                assertThat(
                        sharedDataAndActions.getRegisterPage().getRegisterUsernameErrorMessage()
                ).contains(error);
                break;
            default:
                Fail.fail("Incorrect registration type {}", type);
        }
    }

    @When("^I try to edit the user description with '(.*)'$")
    public void iEditTheUserDescriptionWithTestDescription(String description) throws Throwable {
        if (sharedDataAndActions.getProfilePage().getProfileDetails().canEditDescription()) {
            sharedDataAndActions.getProfilePage().getProfileDetails().editDescription(description);
        }
    }

    @Given("^I register (\\d+) different users$")
    public void iRegisterDifferentUsers(int count) throws Throwable {
        for (int pos = 0; pos < count; pos++) {
            iRegisterANewUser();
        }
    }

    @And("^I login with user (\\d+)$")
    public void iLoginWithUser(int id) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);
        iLoginWithTheCurrentUser();
    }

    @And("^I go to the user (\\d+) profile$")
    public void iGoToTheUserProfile(int id) throws Throwable {
        if (id < 1) {
            Fail.fail("User id must be greater or equal to 1.");
        }

        Optional<UserModel> userOptional = Optional.ofNullable(sharedDataAndActions.getUsers().get(id - 1));

        if (userOptional.isPresent()) {
            sharedDataAndActions.getProfilePage().goToProfile(userOptional.get().getUsername());
        } else {
            Fail.fail("No user has been registered with id {}", id);
        }
    }

    @Then("^the current user description has been edited successfully with '(.*)'$")
    public void theCurrentUserDescriptionHasBeenEditedSuccessfullyWithDescription(String description) throws Throwable {
        assertThat(
                sharedDataAndActions.getProfilePage().getProfileDetails().getProfileDescription()
        ).isEqualTo(description);
    }

    @Then("^I cannot edit other users description$")
    public void iCannotEditOtherUsersDescription() throws Throwable {
        assertThat(
                sharedDataAndActions.getProfilePage().getProfileDetails().canEditDescription()
        ).isFalse();
    }

    @When("^I post a tweet with content '(.*)'$")
    public void iPostATweetWithContent(String tweetContent) throws Throwable {
        sharedDataAndActions.getProfilePage().getProfileTweets().addNewTweet(tweetContent);
    }

    @Then("^a tweet has been posted successfully with content '(.*)'$")
    public void aTweetHasBeenPostedSuccessfullyWithContentTestTweet(String tweetContent) throws Throwable {
        checkTweetPosted(tweetContent);
    }

    @Then("^a tweet for user (\\d+) has been posted successfully with content '(.*)'$")
    public void aTweetHasBeenPostedSuccessfullyWithContentTestTweet(int id, String tweetContent) throws Throwable {
        sharedDataAndActions.setCurrentUserBy(id);
        checkTweetPosted(tweetContent);
    }

    private void checkTweetPosted(String tweetContent) {
        List<TweetModel> tweetList = sharedDataAndActions.getProfilePage().getProfileTweets().getAllProfileTweets();

        logger().info("Checking new tweet is successfully posted");
        assertThat(tweetList).isNotEmpty();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase(tweetContent))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Fail.fail("No tweet has been posted");
        } else {
            assertThat(actualTweetContent.get().getUsername()).isEqualTo("@" + sharedDataAndActions.getCurrentUser().getUsername());
            assertThat(actualTweetContent.get().getLikeCount()).isZero();
        }
    }

    @And("^I (unlike|like) tweets with content '(.*)'$")
    public void iLikeUnlikeTweetsWithContent(String status, String tweetContent) throws Throwable {
        List<TweetModel> tweetList = sharedDataAndActions
                .getProfilePage()
                .getProfileTweets()
                .getAllProfileTweets()
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase(tweetContent))
                .collect(Collectors.toList());

        assertThat(tweetList).overridingErrorMessage("No tweet has been posted").isNotEmpty();

        tweetList.forEach(tweetModel -> {
            if (status.equals("like")) {
                logger().info("Checking if tweet {} has been liked", tweetModel.getId());

                sharedDataAndActions.getProfilePage().getProfileTweets().likeTweet(tweetModel.getId());

                assertThat(sharedDataAndActions.getProfilePage().getProfileTweets().isTweetLiked(tweetModel.getId())).isTrue();
            } else {
                logger().info("Checking if tweet {} has been unliked", tweetModel.getId());

                sharedDataAndActions.getProfilePage().getProfileTweets().unlikeTweet(tweetModel.getId());

                assertThat(sharedDataAndActions.getProfilePage().getProfileTweets().isTweetLiked(tweetModel.getId())).isFalse();
            }
        });
    }

    @Then("^the tweets with content '(.*)' have been successfully (unliked|liked)$")
    public void theTweetsWithContentHaveBeenSuccessfullyLiked(String tweetContent, String status) throws Throwable {
        //Flavour step
    }

    @When("^I (unfollow|follow) user (\\d+)$")
    public void iFollowUser(String status, int id) throws Throwable {
        iGoToTheUserProfile(id);

        sharedDataAndActions.getProfilePage().getProfileDetails().followUser();

        if (Objects.equals(status, "follow")) {
            softAssertions.assertThat(sharedDataAndActions.getProfilePage().getProfileDetails().isFollowed()).isTrue();

            logger().info("Check if user {} is in follower list", sharedDataAndActions.getCurrentUser().getUsername());
            List<FollowerModel> followers = sharedDataAndActions.getProfilePage().getProfileFollowers().getAllFollowers();

            Optional<FollowerModel> actualFollowerOptional = followers
                    .stream()
                    .filter(followerModel -> followerModel.getUsername().equalsIgnoreCase("@" + sharedDataAndActions.getCurrentUser().getUsername()))
                    .findFirst();

            if (!actualFollowerOptional.isPresent()) {
                softAssertions.fail("User is not followed");
            }
        } else {
            softAssertions.assertThat(sharedDataAndActions.getProfilePage().getProfileDetails().isFollowed()).isFalse();

            logger().info("Check if user {} is not in follower list", sharedDataAndActions.getCurrentUser().getUsername());
            List<FollowerModel> followers = sharedDataAndActions.getProfilePage().getProfileFollowers().getAllFollowers();

            Optional<FollowerModel> actualFollowerOptional = followers
                    .stream()
                    .filter(followerModel -> followerModel.getUsername().equalsIgnoreCase("@" + sharedDataAndActions.getCurrentUser().getUsername()))
                    .findFirst();

            if (actualFollowerOptional.isPresent()) {
                softAssertions.fail("User is still followed");
            }
        }

        softAssertions.assertAll();
    }

    @Then("^the user has been successfully (followed|unfollowed)$")
    public void theUserHasBeenSuccessfullyFollowedUnfollowed(String status) throws Throwable {
        //Flavour step
    }

    @Then("^tweets from user (\\d+) show up on profile$")
    public void tweetsFromUserShowUpOnProfile(int id) throws Throwable {
        if (id < 1) {
            Fail.fail("User id must be greater or equal to 1.");
        }

        Optional<UserModel> userOptional = Optional.ofNullable(sharedDataAndActions.getUsers().get(id - 1));

        if (userOptional.isPresent()) {
            List<TweetModel> tweets = sharedDataAndActions.getProfilePage().getProfileTweets().getAllProfileTweets();

            Optional<TweetModel> tweetModelOptional = tweets
                    .stream()
                    .filter(tweetModel -> Objects.equals(tweetModel.getUsername().replace("@", ""), userOptional.get().getUsername()))
                    .findAny();

            logger().info("Checking if tweets of users followed are present");
            assertThat(tweetModelOptional.isPresent()).isTrue();

        } else {
            Fail.fail("No user has been registered with id {}", id);
        }
    }

    @When("^I search for '(.*)'$")
    public void iSearchFor(String searchValue) throws Throwable {
        sharedDataAndActions.getMainHeader().doSearch(searchValue);
    }

    @Then("^search results are (not |)present for value '(.*)'$")
    public void searchResultsArePresentForValue(String status, String searchValue) throws Throwable {
        assertThat(sharedDataAndActions.getSearchPage().isDisplayed()).isTrue();

        if (Objects.equals(status.trim(), "not")) {
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getSearchUsersText()).isEqualTo("No users found");
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getSearchTweetsText()).isEqualTo("No tweets found");
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getAllSearchUsers()).isEmpty();
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getAllSearchTweets()).isEmpty();

        } else {
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getSearchUsersText()).isEqualTo("Users found");
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getSearchTweetsText()).isEqualTo("Tweets found");
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getAllSearchUsers()).isNotEmpty();
            softAssertions.assertThat(sharedDataAndActions.getSearchPage().getAllSearchTweets()).isNotEmpty();
        }

        softAssertions.assertAll();
    }
}
