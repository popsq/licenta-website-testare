package com.andrei.licenta.testautomation.junit.web.profile;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import com.andrei.licenta.testautomation.model.TweetModel;
import com.andrei.licenta.testautomation.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class TweetTest extends TestBase {

    @Test void postNewTweet(){
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));
        Assertions.assertTrue(profilePage.getProfileTweets().noTweetsPresent());

        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        logger().info("Checking new tweet is successfully posted");
        Assertions.assertEquals(1, tweetList.size());

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()){
            Assertions.fail("No tweet has been posted");
        } else {
            Assertions.assertEquals("@" + userModel.getUsername(), actualTweetContent.get().getUsername());
            Assertions.assertEquals(0, actualTweetContent.get().getLikeCount());
        }
    }

    @Test void viewTweetOnOtherProfile(){
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));

        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        mainHeader.logout();

        loginPage.doLogin("admin@licenta.fmi", "licenta");
        profilePage.goToProfile(userModel.getUsername());

        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        logger().info("Checking new tweet is successfully posted");
        Assertions.assertEquals(1, tweetList.size());

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()){
            Assertions.fail("No tweet has been posted");
        } else {
            Assertions.assertEquals("@" + userModel.getUsername(), actualTweetContent.get().getUsername());
            Assertions.assertEquals(0, actualTweetContent.get().getLikeCount());
        }
    }

    @Test void likeOwnTweet(){
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));

        profilePage.getProfileTweets().addNewTweet("Test Tweet");
        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()){
            logger().info("Checking if tweet has been liked");
            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            Assertions.assertTrue(profilePage.getProfileTweets().isTweetLiked(actualTweetContent.get().getId()));
            Assertions.assertEquals("1", profilePage.getProfileTweets().getTweetCount(actualTweetContent.get().getId()));
        } else {
            Assertions.fail("No tweet has been posted");
        }
    }

    @Test void unlikeOwnTweet(){
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));

        profilePage.getProfileTweets().addNewTweet("Test Tweet");
        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()){
            logger().info("Checking if tweet has been liked");
            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            profilePage.getProfileTweets().unlikeTweet(actualTweetContent.get().getId());

            Assertions.assertFalse(profilePage.getProfileTweets().isTweetLiked(actualTweetContent.get().getId()));
            Assertions.assertEquals("0", profilePage.getProfileTweets().getTweetCount(actualTweetContent.get().getId()));
        } else {
            Assertions.fail("No tweet has been posted");
        }
    }

    @Test void likeOtherUserTweet(){
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        mainHeader.logout();
        loginPage.goToRegisterPage();
        registerPage.doRegister(user2);
        profilePage.goToProfile(user1.getUsername());

        Assertions.assertTrue(profilePage.isCorrectProfile(user1.getUsername()));

        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()){
            logger().info("Checking if tweet has been liked");
            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            Assertions.assertTrue(profilePage.getProfileTweets().isTweetLiked(actualTweetContent.get().getId()));
            Assertions.assertEquals("1", profilePage.getProfileTweets().getTweetCount(actualTweetContent.get().getId()));
        } else {
            Assertions.fail("No tweet has been posted");
        }
    }

    @Test void unlikeOtherUserTweet(){
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        mainHeader.logout();
        loginPage.goToRegisterPage();
        registerPage.doRegister(user2);
        profilePage.goToProfile(user1.getUsername());

        Assertions.assertTrue(profilePage.isCorrectProfile(user1.getUsername()));

        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()){
            logger().info("Checking if tweet has been liked");
            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            profilePage.getProfileTweets().unlikeTweet(actualTweetContent.get().getId());

            Assertions.assertFalse(profilePage.getProfileTweets().isTweetLiked(actualTweetContent.get().getId()));
            Assertions.assertEquals("0", profilePage.getProfileTweets().getTweetCount(actualTweetContent.get().getId()));
        } else {
            Assertions.fail("No tweet has been posted");
        }
    }

    @Test void tweetMultipleLikes() {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();
        UserModel user3 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        profilePage.getProfileTweets().addNewTweet("Test Tweet");

        List<TweetModel> tweetList = profilePage.getProfileTweets().getAllProfileTweets();

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweetModel -> tweetModel.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()) {
            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            mainHeader.logout();
            loginPage.goToRegisterPage();
            registerPage.doRegister(user2);
            profilePage.goToProfile(user1.getUsername());

            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            mainHeader.logout();
            loginPage.goToRegisterPage();
            registerPage.doRegister(user3);
            profilePage.goToProfile(user1.getUsername());

            profilePage.getProfileTweets().likeTweet(actualTweetContent.get().getId());

            Assertions.assertTrue(profilePage.getProfileTweets().isTweetLiked(actualTweetContent.get().getId()));
            Assertions.assertEquals("3", profilePage.getProfileTweets().getTweetCount(actualTweetContent.get().getId()));
        } else {
            Assertions.fail("No tweet has been posted");
        }
    }

}
