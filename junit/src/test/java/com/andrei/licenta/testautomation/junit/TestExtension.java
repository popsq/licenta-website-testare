package com.andrei.licenta.testautomation.junit;

import com.andrei.licenta.testautomation.utils.LogProvider;
import org.junit.jupiter.api.extension.AfterTestExecutionCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

public class TestExtension implements AfterTestExecutionCallback, LogProvider {

    @Override
    public void afterTestExecution(ExtensionContext extensionContext) throws Exception {
        String testResult;
        if (extensionContext.getExecutionException().isPresent()){
            testResult = "FAILED";
        } else {
            testResult = "SUCCESS";
        }

        String testName = extensionContext.getDisplayName();

        logger().info("Test [{}] result: [{}]", testName, testResult);
    }

}
