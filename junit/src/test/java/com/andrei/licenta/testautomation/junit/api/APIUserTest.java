package com.andrei.licenta.testautomation.junit.api;

import com.andrei.licenta.testautomation.model.*;
import com.andrei.licenta.testautomation.utils.JSONUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

public class APIUserTest extends APITestBase {

    @Test
    void registerUserAPI() throws IOException {
        UserModel user = UserModel.generateRandomUser();

        GenericResponse registerResponse = userRequestHandler.registerUser(user);

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_CREATION_SUCCESS, registerResponse.getResult().getCode())
        );
    }

    @Test
    void registerUserErrorUsernameAPI() {
        UserModel user = UserModel.generateRandomUser();

        AtomicReference<GenericResponse> registerResponse = new AtomicReference<>();
        AtomicReference<List<ActionResult>> errorDetails = new AtomicReference<>();

        Assertions.assertAll(
                () -> {
                    user.setUsername("");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid username.")));
                },
                () -> {
                    user.setUsername("admin");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "User already exists.")));
                }
        );
    }

    @Test
    void registerUserErrorEmailAPI() {
        UserModel user = UserModel.generateRandomUser();

        AtomicReference<GenericResponse> registerResponse = new AtomicReference<>();
        AtomicReference<List<ActionResult>> errorDetails = new AtomicReference<>();

        Assertions.assertAll(
                () -> {
                    user.setEmail("");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    user.setEmail("admin");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    user.setEmail("admin@");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    user.setEmail("admin@fmi.");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Invalid Email.")));
                },
                () -> {
                    user.setEmail("admin@licenta.fmi");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Email has been registered to another user.")));
                }
        );
    }

    @Test
    void registerUserErrorPassword() {
        UserModel user = UserModel.generateRandomUser();

        AtomicReference<GenericResponse> registerResponse = new AtomicReference<>();
        AtomicReference<List<ActionResult>> errorDetails = new AtomicReference<>();

        Assertions.assertAll(
                () -> {
                    user.setPassword("");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password or matching password cannot be empty.")));
                },
                () -> {
                    user.setPassword("admin");
                    user.setMatchingPassword("admin1");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Retyped password does not match provided password.")));
                },
                () -> {
                    user.setPassword("thisisaverylongpassword");
                    user.setMatchingPassword("thisisaverylongpassword");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should be less than 20 characters in length.")));
                },
                () -> {
                    user.setPassword("short");
                    user.setMatchingPassword("short");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should be more than 8 characters in length.")));
                },
                () -> {
                    user.setPassword(user.getUsername());
                    user.setPassword(user.getUsername());
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should not be the same or contain the username.")));
                },
                () -> {
                    user.setPassword("1111");
                    user.setMatchingPassword("1111");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one upper case alphabetic character.")));
                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one lower case alphabetic character.")));
                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one special character.")));
                },
                () -> {
                    user.setPassword("aaaaA!");
                    user.setMatchingPassword("aaaaA!");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one numeric character.")));
                },
                () -> {
                    user.setPassword("!@$#@%$%$#!");
                    user.setMatchingPassword("!@$#@%$%$#!");
                    registerResponse.set(userRequestHandler.registerUser(user));
                    Assertions.assertEquals(ActionResultCode.USER_CREATION_FAIL, registerResponse.get().getResult().getCode());

                    errorDetails.set(JSONUtils.getJsonMapper().convertValue(
                            registerResponse.get().getDetails(),
                            new TypeReference<List<ActionResult>>() {
                            })
                    );

                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one upper case alphabetic character.")));
                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one lower case alphabetic character.")));
                    Assertions.assertTrue(errorDetails.get().stream().anyMatch(actionResult -> Objects.equals(actionResult.getReason(), "Password should contain at least one numeric character.")));
                }
        );
    }

    @Test
    void canEditOwnProfileDescriptionAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        userRequestHandler.setAuthenticationModel(auth);

        user.setDescription("This is a test description.");

        userRequestHandler.editDescription(user);
        GenericResponse response = userRequestHandler.getUserDetails(user);

        UserModel userDetails = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        Assertions.assertEquals("This is a test description.", userDetails.getDescription());
    }

    @Test
    void cannotEditOtherProfileDescriptionsAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel("admin@licenta.fmi", "licenta");

        userRequestHandler.registerUser(user);
        userRequestHandler.setAuthenticationModel(auth);

        user.setDescription("This is a test description.");

        GenericResponse response = userRequestHandler.editDescription(user);

        Assertions.assertEquals("This user cannot edit details for another user.", response.getResult().getReason());
        Assertions.assertEquals(ActionResultCode.ERROR, response.getResult().getCode());
    }

    @Test
    void followUserAPI() throws JsonProcessingException {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        AuthenticationModel auth1 = new AuthenticationModel(user1.getEmail(), user1.getPassword());
        AuthenticationModel auth2 = new AuthenticationModel(user2.getEmail(), user2.getPassword());

        userRequestHandler.registerUser(user1);
        userRequestHandler.registerUser(user2);

        userRequestHandler.setAuthenticationModel(auth1);
        user1 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user1).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.setAuthenticationModel(auth2);
        user2 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user2).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.setAuthenticationModel(auth1);
        GenericResponse response = userRequestHandler.followUser(user2);
        List<FollowerModel> followers = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getFollowers(user2).getDetails(),
                new TypeReference<List<FollowerModel>>() {
                }
        );

        UserModel finalUser1 = user1;
        UserModel finalUser2 = user2;
        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_FOLLOW_SUCCESS, response.getResult().getCode()),
                () -> Assertions.assertEquals("User follow successful.", response.getResult().getReason()),
                () -> {
                    logger().info("Check if user {} is in follower list", finalUser1.getUsername());

                    Optional<FollowerModel> actualFollowerOptional = followers
                            .stream()
                            .filter(followerModel -> followerModel.getUser().getUsername().equalsIgnoreCase(finalUser1.getUsername()))
                            .findFirst();

                    if (!actualFollowerOptional.isPresent()) {
                        Assertions.fail("User " + finalUser2.getUsername() + " not followed by " + finalUser1.getUsername());
                    }
                }
        );
    }

    @Test
    void followUserDuplicateAPI() throws JsonProcessingException {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        AuthenticationModel auth1 = new AuthenticationModel(user1.getEmail(), user1.getPassword());

        userRequestHandler.registerUser(user1);
        userRequestHandler.registerUser(user2);

        userRequestHandler.setAuthenticationModel(auth1);
        user2 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user2).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.followUser(user2);
        GenericResponse response = userRequestHandler.followUser(user2);
        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_FOLLOW_DUPLICATE, response.getResult().getCode()),
                () -> Assertions.assertEquals("User already followed.", response.getResult().getReason())
        );
    }

    @Test
    void unfollowUserAPI() throws JsonProcessingException {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        AuthenticationModel auth1 = new AuthenticationModel(user1.getEmail(), user1.getPassword());
        AuthenticationModel auth2 = new AuthenticationModel(user2.getEmail(), user2.getPassword());

        userRequestHandler.registerUser(user1);
        userRequestHandler.registerUser(user2);

        userRequestHandler.setAuthenticationModel(auth1);
        user1 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user1).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.setAuthenticationModel(auth2);
        user2 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user2).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.setAuthenticationModel(auth1);
        userRequestHandler.followUser(user2);
        GenericResponse response = userRequestHandler.unfollowUser(user2);
        List<FollowerModel> followers = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getFollowers(user2).getDetails(),
                new TypeReference<List<FollowerModel>>() {
                }
        );

        UserModel finalUser1 = user1;
        UserModel finalUser2 = user2;
        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_UNFOLLOW_SUCCESS, response.getResult().getCode()),
                () -> Assertions.assertEquals("User unfollow successful.", response.getResult().getReason()),
                () -> {
                    logger().info("Check if user {} is not in follower list", finalUser1.getUsername());

                    Optional<FollowerModel> actualFollowerOptional = followers
                            .stream()
                            .filter(followerModel -> followerModel.getUser().getUsername().equalsIgnoreCase(finalUser1.getUsername()))
                            .findFirst();

                    if (actualFollowerOptional.isPresent() && actualFollowerOptional.get().isActive()) {
                        Assertions.fail("User " + finalUser2.getUsername() + " is followed by " + finalUser1.getUsername());
                    }
                }
        );
    }

    @Test
    void unfollowUserDuplicateAPI() throws JsonProcessingException {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        AuthenticationModel auth1 = new AuthenticationModel(user1.getEmail(), user1.getPassword());

        userRequestHandler.registerUser(user1);
        userRequestHandler.registerUser(user2);

        userRequestHandler.setAuthenticationModel(auth1);
        user2 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user2).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        userRequestHandler.followUser(user2);
        userRequestHandler.unfollowUser(user2);
        GenericResponse response = userRequestHandler.unfollowUser(user2);
        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_UNFOLLOW_DUPLICATE, response.getResult().getCode()),
                () -> Assertions.assertEquals("User already unfollowed.", response.getResult().getReason())
        );
    }


    @Test
    void unfollowUserFailureAPI() throws JsonProcessingException {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        AuthenticationModel auth1 = new AuthenticationModel(user1.getEmail(), user1.getPassword());

        userRequestHandler.registerUser(user1);
        userRequestHandler.registerUser(user2);

        userRequestHandler.setAuthenticationModel(auth1);
        user2 = JSONUtils.getJsonMapper().convertValue(
                userRequestHandler.getUserDetails(user2).getDetails(),
                new TypeReference<UserModel>() {
                }
        );

        GenericResponse response = userRequestHandler.unfollowUser(user2);
        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.USER_UNFOLLOW_FAILURE, response.getResult().getCode()),
                () -> Assertions.assertEquals("User must be followed first.", response.getResult().getReason())
        );
    }

    @Test
    void searchUsersAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());
        userRequestHandler.registerUser(user);

        searchRequestHandler.setAuthenticationModel(auth);

        GenericResponse response = searchRequestHandler.searchUsers(user.getUsername());
        List<UserSearchModel> searchResults = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<UserSearchModel>>() {
                }
        );

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.OK, response.getResult().getCode()),
                () -> Assertions.assertEquals("Search result retrieved.", response.getResult().getReason()),
                () -> Assertions.assertTrue(searchResults.size() != 0)
        );
    }

    @Test
    void searchUsersNoResultsAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());
        userRequestHandler.registerUser(user);

        searchRequestHandler.setAuthenticationModel(auth);

        GenericResponse response = searchRequestHandler.searchUsers("z");
        List<UserSearchModel> searchResults = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<UserSearchModel>>() {
                }
        );

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.OK, response.getResult().getCode()),
                () -> Assertions.assertEquals("Search result retrieved.", response.getResult().getReason()),
                () -> Assertions.assertEquals(0, searchResults.size())
        );
    }

}
