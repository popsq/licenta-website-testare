package com.andrei.licenta.testautomation.junit.web.profile;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import com.andrei.licenta.testautomation.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

public class ProfileTest extends TestBase {

    private DateTimeFormatter dateTimeFormatterProfile = DateTimeFormatter.ofPattern("dd-MMM-yyyy");

    @Test
    void defaultProfileCorrect() {
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);
        String userCreationTimestamp = Instant
                .ofEpochMilli(Instant.now().toEpochMilli())
                .atZone(ZoneId.of("GMT+0"))
                .format(dateTimeFormatterProfile);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));

        Assertions.assertAll(
                () -> Assertions.assertEquals(userModel.getUsername(), profilePage.getProfileDetails().getProfileUsername()),
                () -> Assertions.assertEquals(userModel.getEmail(), profilePage.getProfileDetails().getProfileEmail()),
                () -> Assertions.assertEquals("Add your own description.", profilePage.getProfileDetails().getProfileDescription()),
                () -> Assertions.assertEquals(userCreationTimestamp, profilePage.getProfileDetails().getProfileJoinDate()),
                () -> Assertions.assertTrue(profilePage.getProfileTweets().noTweetsPresent()),
                () -> Assertions.assertTrue(profilePage.getProfileFollowers().noFollowersPresent()),
                () -> Assertions.assertEquals("@" + userModel.getUsername(), mainHeader.getUserText())
        );
    }

    @Test
    void canEditOwnProfileDescription() {
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));
        Assertions.assertEquals("Add your own description.", profilePage.getProfileDetails().getProfileDescription());

        String testDescription = "This is a test description.";

        profilePage.getProfileDetails().editDescription(testDescription);

        Assertions.assertEquals(testDescription, profilePage.getProfileDetails().getProfileDescription());
    }

    @Test
    void cannotEditOtherProfileDescriptions() {
        UserModel userModel = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(userModel);

        Assertions.assertTrue(profilePage.isCorrectProfile(userModel.getUsername()));
        Assertions.assertTrue(profilePage.getProfileDetails().canEditDescription());

        profilePage.goToProfile("admin");

        Assertions.assertFalse(profilePage.getProfileDetails().canEditDescription());
    }
}

