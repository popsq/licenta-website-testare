package com.andrei.licenta.testautomation.junit.web.profile;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import com.andrei.licenta.testautomation.model.FollowerModel;
import com.andrei.licenta.testautomation.model.TweetModel;
import com.andrei.licenta.testautomation.model.UserModel;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class FollowTest extends TestBase {

    @Test
    void followUser() {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        mainHeader.logout();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user2);
        profilePage.goToProfile(user1.getUsername());

        profilePage.getProfileDetails().followUser();

        Assertions.assertAll(
                () -> Assertions.assertTrue(profilePage.getProfileDetails().isFollowed()),
                () -> {
                    logger().info("Check if user {} is in follower list", user2.getUsername());
                    List<FollowerModel> followers = profilePage.getProfileFollowers().getAllFollowers();

                    Optional<FollowerModel> actualFollowerOptional = followers
                            .stream()
                            .filter(followerModel -> followerModel.getUsername().equalsIgnoreCase("@" + user2.getUsername()))
                            .findFirst();

                    if (!actualFollowerOptional.isPresent()) {
                        Assertions.fail("User " + user1.getUsername() + " not followed by " + user2.getUsername());
                    }
                }
        );
    }

    @Test
    void unfollowUser() {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        mainHeader.logout();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user2);
        profilePage.goToProfile(user1.getUsername());

        //Follow
        profilePage.getProfileDetails().followUser();

        //Unfollow
        profilePage.getProfileDetails().followUser();

        Assertions.assertAll(
                () -> Assertions.assertFalse(profilePage.getProfileDetails().isFollowed()),
                () -> {
                    logger().info("Check if user {} is not in follower list", user2.getUsername());
                    List<FollowerModel> followers = profilePage.getProfileFollowers().getAllFollowers();

                    Optional<FollowerModel> actualFollowerOptional = followers
                            .stream()
                            .filter(followerModel -> followerModel.getUsername().equalsIgnoreCase("@" + user2.getUsername()))
                            .findFirst();

                    if (actualFollowerOptional.isPresent()) {
                        Assertions.fail("User " + user1.getUsername() + " is still followed by " + user2.getUsername());
                    }
                }
        );
    }

    @Test
    void followedTweets() {
        UserModel user1 = UserModel.generateRandomUser();
        UserModel user2 = UserModel.generateRandomUser();

        loginPage.goToRegisterPage();
        registerPage.doRegister(user1);
        profilePage.getProfileTweets().addNewTweet("Test Tweet");
        mainHeader.logout();
        loginPage.goToRegisterPage();
        registerPage.doRegister(user2);
        profilePage.getProfileTweets().addNewTweet("Test Tweet");
        profilePage.goToProfile(user1.getUsername());
        profilePage.getProfileDetails().followUser();
        mainHeader.logout();
        loginPage.doLogin(user1);
        profilePage.goToProfile(user2.getUsername());
        profilePage.getProfileDetails().followUser();
        profilePage.goToProfile(user1.getUsername());

        Assertions.assertAll(
                () -> {
                    List<TweetModel> tweets = profilePage.getProfileTweets().getAllProfileTweets();

                    Optional<TweetModel> tweetModelOptional = tweets
                            .stream()
                            .filter(tweetModel -> Objects.equals(tweetModel.getUsername().replace("@", ""), user2.getUsername()))
                            .findAny();

                    Assertions.assertTrue(tweetModelOptional.isPresent(), "Checking if tweets of users followed are present");
                },
                () -> {
                    logger().info("Check if user {} is not in follower list", user2.getUsername());
                    List<FollowerModel> followers = profilePage.getProfileFollowers().getAllFollowers();

                    Optional<FollowerModel> followerModelOptional = followers
                            .stream()
                            .filter(tweetModel -> Objects.equals(tweetModel.getUsername().replace("@", ""), user2.getUsername()))
                            .findAny();

                    if (followerModelOptional.isPresent()){
                        Assertions.assertTrue(followerModelOptional.get().isFollowed());
                    } else {
                        Assertions.fail("User " + user1.getUsername() + " not followed by " + user2.getUsername());
                    }
                }
        );

    }
}
