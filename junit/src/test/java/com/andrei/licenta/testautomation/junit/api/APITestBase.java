package com.andrei.licenta.testautomation.junit.api;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.handlers.SearchRequestHandler;
import com.andrei.licenta.testautomation.api.handlers.TweetRequestHandler;
import com.andrei.licenta.testautomation.api.handlers.UserRequestHandler;
import com.andrei.licenta.testautomation.junit.TestExtension;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(TestExtension.class)
public class APITestBase implements LogProvider {

    private static APIConnection apiConnection = new APIConnection();

    protected static UserRequestHandler userRequestHandler;
    protected static TweetRequestHandler tweetRequestHandler;
    protected static SearchRequestHandler searchRequestHandler;

    @BeforeAll
    public static void setup() {
        apiConnection.setBaseURI("http://localhost");
        apiConnection.setPort(8080);

        userRequestHandler = new UserRequestHandler(apiConnection);
        tweetRequestHandler = new TweetRequestHandler(apiConnection);
        searchRequestHandler = new SearchRequestHandler(apiConnection);
    }

    @BeforeEach
    public void init(TestInfo testInfo){
        logger().info("Starting test: [{}]", testInfo.getDisplayName());
    }

    @AfterEach
    public void cleanup(){
        userRequestHandler.cleanup();

        userRequestHandler.setAuthenticationModel(null);
        tweetRequestHandler.setAuthenticationModel(null);
        searchRequestHandler.setAuthenticationModel(null);
        apiConnection.resetHeaders();
    }

}
