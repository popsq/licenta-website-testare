package com.andrei.licenta.testautomation.junit.web.entry;

import com.andrei.licenta.testautomation.junit.web.TestBase;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class LoginTest extends TestBase {

    @Test void testLogin() {
        loginPage.doLogin("admin@licenta.fmi", "licenta");
        Assertions.assertTrue(profilePage.isCorrectProfile("admin"));
    }

    @Test void testLoginHTML5Error() {
        Assertions.assertAll(
                () -> {
                    loginPage.doLogin("admin", "licenta");
                    Assertions.assertTrue(loginPage.isValidationTriggered());
                },
                () -> {
                    loginPage.doLogin("admin@", "licenta");
                    Assertions.assertTrue(loginPage.isValidationTriggered());
                },
                () -> {
                    loginPage.doLogin("admin@fmi.", "licenta");
                    Assertions.assertTrue(loginPage.isValidationTriggered());
                }
        );
    }

    @Test void testLoginError() {
        Assertions.assertAll(
                () -> {
                    loginPage.doLogin("admin@test", "licenta");
                    Assertions.assertEquals("Invalid email or password.", loginPage.getLoginErrorMessage());
                },
                () -> {
                    loginPage.doLogin("admin@licenta.fmi", "licenta1");
                    Assertions.assertEquals("Invalid email or password.", loginPage.getLoginErrorMessage());
                }
        );
    }
}
