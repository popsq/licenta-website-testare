package com.andrei.licenta.testautomation.junit.api;

import com.andrei.licenta.testautomation.model.*;
import com.andrei.licenta.testautomation.utils.JSONUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

public class APITweetTest extends APITestBase {

    @Test
    void postNewTweetAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        logger().info("Checking new tweet is successfully posted");
        Assertions.assertEquals(1, tweetList.size());

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Assertions.fail("No tweet has been posted");
        } else {
            Assertions.assertEquals(user.getUsername(), actualTweetContent.get().getUser().getUsername());
            Assertions.assertEquals(0, actualTweetContent.get().getLikeCount());
        }
    }

    @Test
    void cannotPostNewTweetForOtherUserAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel("admin@licenta.fmi", "licenta");

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        logger().info("Checking new tweet has not been posted");
        Assertions.assertEquals(0, tweetList.size());

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (actualTweetContent.isPresent()) {
            Assertions.fail("Tweet has been posted");
        }
    }

    @Test
    void likeTweetAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Assertions.fail("No tweet has been posted");
        } else {
            response = tweetRequestHandler.likeTweet(actualTweetContent.get());

            GenericResponse finalResponse = response;
            Assertions.assertAll(
                    () -> Assertions.assertEquals(ActionResultCode.TWEET_LIKE_SUCCESS, finalResponse.getResult().getCode()),
                    () -> Assertions.assertEquals("Tweet like successful.", finalResponse.getResult().getReason())
            );
        }
    }

    @Test
    void likeTweetDuplicateAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Assertions.fail("No tweet has been posted");
        } else {
            tweetRequestHandler.likeTweet(actualTweetContent.get());
            response = tweetRequestHandler.likeTweet(actualTweetContent.get());

            GenericResponse finalResponse = response;
            Assertions.assertAll(
                    () -> Assertions.assertEquals(ActionResultCode.TWEET_LIKE_DUPLICATE, finalResponse.getResult().getCode()),
                    () -> Assertions.assertEquals("Tweet already liked.", finalResponse.getResult().getReason())
            );
        }
    }

    @Test
    void likeTweetNotExistsAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setId("999999999999");

        GenericResponse response = tweetRequestHandler.likeTweet(tweetModel);

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.TWEET_LIKE_FAILURE, response.getResult().getCode()),
                () -> Assertions.assertEquals("Tweet does not exist.", response.getResult().getReason())
        );
    }

    @Test
    void unlikeTweetAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Assertions.fail("No tweet has been posted");
        } else {
            tweetRequestHandler.likeTweet(actualTweetContent.get());
            response = tweetRequestHandler.unlikeTweet(actualTweetContent.get());

            GenericResponse finalResponse = response;
            Assertions.assertAll(
                    () -> Assertions.assertEquals(ActionResultCode.TWEET_UNLIKE_SUCCESS, finalResponse.getResult().getCode()),
                    () -> Assertions.assertEquals("Tweet unlike successful.", finalResponse.getResult().getReason())
            );
        }
    }

    @Test
    void unlikeTweetDuplicateAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = tweetRequestHandler.getTweets(user);

        List<TweetModel> tweetList = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetModel>>() {
                }
        );

        Optional<TweetModel> actualTweetContent = tweetList
                .stream()
                .filter(tweet -> tweet.getContent().equalsIgnoreCase("Test Tweet"))
                .findFirst();

        if (!actualTweetContent.isPresent()) {
            Assertions.fail("No tweet has been posted");
        } else {
            tweetRequestHandler.likeTweet(actualTweetContent.get());
            tweetRequestHandler.unlikeTweet(actualTweetContent.get());
            response = tweetRequestHandler.unlikeTweet(actualTweetContent.get());

            GenericResponse finalResponse = response;
            Assertions.assertAll(
                    () -> Assertions.assertEquals(ActionResultCode.TWEET_UNLIKE_DUPLICATE, finalResponse.getResult().getCode()),
                    () -> Assertions.assertEquals("Tweet already unlike.", finalResponse.getResult().getReason())
            );
        }
    }

    @Test
    void unlikeTweetFailureAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());

        userRequestHandler.registerUser(user);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setId("0");

        GenericResponse response = tweetRequestHandler.unlikeTweet(tweetModel);

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.TWEET_UNLIKE_FAILURE, response.getResult().getCode()),
                () -> Assertions.assertEquals("Tweet needs to be liked first.", response.getResult().getReason())
        );
    }

    @Test
    void searchTweetsAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());
        userRequestHandler.registerUser(user);

        searchRequestHandler.setAuthenticationModel(auth);
        tweetRequestHandler.setAuthenticationModel(auth);

        TweetModel tweetModel = new TweetModel();
        tweetModel.setContent("Test Tweet");
        tweetModel.setUsername(user.getUsername());
        tweetRequestHandler.addTweet(tweetModel);

        GenericResponse response = searchRequestHandler.searchTweets(tweetModel.getContent());
        List<TweetSearchModel> searchResults = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<UserSearchModel>>() {
                }
        );

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.OK, response.getResult().getCode()),
                () -> Assertions.assertEquals("Search result retrieved.", response.getResult().getReason()),
                () -> Assertions.assertTrue(searchResults.size() != 0)
        );
    }

    @Test
    void searchTweetsNoResultsAPI() throws JsonProcessingException {
        UserModel user = UserModel.generateRandomUser();
        AuthenticationModel auth = new AuthenticationModel(user.getEmail(), user.getPassword());
        userRequestHandler.registerUser(user);

        searchRequestHandler.setAuthenticationModel(auth);
        tweetRequestHandler.setAuthenticationModel(auth);

        GenericResponse response = searchRequestHandler.searchTweets("z");
        List<TweetSearchModel> searchResults = JSONUtils.getJsonMapper().convertValue(
                response.getDetails(),
                new TypeReference<List<TweetSearchModel>>() {
                }
        );

        Assertions.assertAll(
                () -> Assertions.assertEquals(ActionResultCode.OK, response.getResult().getCode()),
                () -> Assertions.assertEquals("Search result retrieved.", response.getResult().getReason()),
                () -> Assertions.assertEquals(0, searchResults.size())
        );
    }

}
