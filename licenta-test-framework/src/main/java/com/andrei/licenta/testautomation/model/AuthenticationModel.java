package com.andrei.licenta.testautomation.model;

public class AuthenticationModel {

    private String email;
    private String password;

    public AuthenticationModel(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }
}
