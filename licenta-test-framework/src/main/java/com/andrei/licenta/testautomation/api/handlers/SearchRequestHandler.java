package com.andrei.licenta.testautomation.api.handlers;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.api.APIPaths;
import com.andrei.licenta.testautomation.model.GenericResponse;

public class SearchRequestHandler extends RequestHandler {
    public SearchRequestHandler(APIConnection apiConnection) {
        super(apiConnection);
    }

    public GenericResponse searchUsers(String searchValue){
        logger().info("Searching users with name like '{}'", searchValue);
        getApiConnection()
                .addQueryParams("value", searchValue)
                .setBasePath(APIPaths.searchUser)
                .sendGet();

        return getApiConnectionResponse();
    }

    public GenericResponse searchTweets(String searchValue){
        logger().info("Searching tweets with content containing '{}'", searchValue);
        getApiConnection()
                .addQueryParams("value", searchValue)
                .setBasePath(APIPaths.searchTweet)
                .sendGet();

        return getApiConnectionResponse();
    }
}
