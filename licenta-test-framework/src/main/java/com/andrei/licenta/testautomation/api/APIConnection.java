package com.andrei.licenta.testautomation.api;

import com.andrei.licenta.testautomation.utils.LogProvider;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.config.RestAssuredConfig;
import io.restassured.http.ContentType;
import io.restassured.parsing.Parser;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.LinkedHashMap;
import java.util.Map;

public class APIConnection implements LogProvider {

    private RequestSpecBuilder requestSpecBuilder;

    //Request data
    private String uri = "http://localhost";
    private String path = "";
    private int port = 8080;
    private Map<String, String> headers = new LinkedHashMap<>();

    //Response data
    private Response latestResponse;

    static {
        RestAssured.config = RestAssuredConfig.config();
        RestAssured.defaultParser = Parser.JSON;
    }

    public APIConnection(){
        requestSpecBuilder = new RequestSpecBuilder();
    }

    public APIConnection setBody(String body, ContentType contentType){
        logger().info("Set request body:\n{}", body);
        requestSpecBuilder.setBody(body);
        requestSpecBuilder.setContentType(contentType);
        return this;
    }

    public APIConnection setBaseURI(String uri){
        this.uri = uri;
        return this;
    }

    public APIConnection setBasePath(String path){
        this.path = path;
        return this;
    }

    public APIConnection setPort(int port){
        this.port = port;
        return this;
    }

    public APIConnection addHeader(String key, String value){
        this.headers.put(key, value);
        return this;
    }

    public APIConnection addQueryParams(String name, String value){
        requestSpecBuilder.addQueryParam(name, value);
        return this;
    }

    private RequestSpecification setupRequest(){
        requestSpecBuilder
                .setBaseUri(this.uri)
                .setBasePath(this.path)
                .setPort(this.port)
                .addHeaders(this.headers);
        return requestSpecBuilder.build();
    }

    private RequestSpecification setupRequest(String uri, String path, int port, Map<String, String> headers){
        requestSpecBuilder
                .setBaseUri(uri)
                .setBasePath(path)
                .setPort(port)
                .addHeaders(headers);
        return requestSpecBuilder.build();
    }

    public APIConnection sendGet(){
        logger().info("Sending GET request {}", this.path);
        logger().info("Auth headers: {}", headers);

        try {
            latestResponse = RestAssured.given(setupRequest()).when().get().thenReturn();
            logResponse();
        } catch (Exception e){
            logger().error("Connection refused to server.");
        }

        reset();
        return this;
    }

    public APIConnection sendPut(){
        logger().info("Sending PUT request {}", this.path);
        logger().info("Auth headers: {}", headers);

        try {
            latestResponse = RestAssured.given(setupRequest()).when().put().thenReturn();
            logResponse();
        } catch (Exception e){
            logger().error("Connection refused to server.");
        }

        reset();
        return this;
    }

    public Response getLatestResponse() {
        return latestResponse;
    }

    public APIConnection sendPost(){
        logger().info("Sending POST request {}", this.path);
        logger().info("Auth headers: {}", headers);

        try {
            latestResponse = RestAssured.given(setupRequest()).when().post().thenReturn();
            logResponse();
        } catch (Exception e){
            logger().error("Connection refused to server.");
        }

        reset();
        return this;
    }

    public APIConnection sendDelete(){
        logger().info("Sending DELETE request {}", this.path);
        logger().info("Auth headers: {}", headers);

        try {
            latestResponse = RestAssured.given(setupRequest()).when().delete().thenReturn();
            logResponse();
        } catch (Exception e){
            logger().error("Connection refused to server.");
        }

        reset();
        return this;
    }

    private void logResponse(){
        logger().info("Response body:\n{}", latestResponse.getBody().prettyPrint());
    }

    private void reset(){
        this.uri = "http://localhost";
        this.path = "";
        this.port = 8080;
        requestSpecBuilder = new RequestSpecBuilder();
    }

    public void resetHeaders(){
        this.headers.clear();
    }

}
