package com.andrei.licenta.testautomation.webdriver.pageobjects;

import org.openqa.selenium.WebDriver;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Profile extends PageObjectBase {

    private ProfileDetails profileDetails;
    private ProfileTweets profileTweets;
    private ProfileFollowers profileFollowers;

    public Profile(WebDriver webDriver) {
        super(webDriver);

        profileDetails = new ProfileDetails(webDriver);
        profileTweets = new ProfileTweets(webDriver);
        profileFollowers = new ProfileFollowers(webDriver);
    }

    public ProfileDetails getProfileDetails() {
        return profileDetails;
    }

    public ProfileTweets getProfileTweets() {
        return profileTweets;
    }

    public ProfileFollowers getProfileFollowers() {
        return profileFollowers;
    }

    public boolean isCorrectProfile(String username) {
        return profileDetails.getProfileUsername().equals(username);
    }

    public void goToProfile(String username) {
        getWebDriver().navigate().to("localhost:8080/u/" + username);
    }
}
