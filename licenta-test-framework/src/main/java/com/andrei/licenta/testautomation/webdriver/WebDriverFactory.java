package com.andrei.licenta.testautomation.webdriver;

import com.andrei.licenta.testautomation.utils.LogProvider;
import com.andrei.licenta.testautomation.webdriver.capabilities.ChromeCapabilities;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class WebDriverFactory implements LogProvider {

    private static Map<String, WebDriver> webDriverMap = new ConcurrentHashMap<>();

    public WebDriverFactory() {
        Runtime.getRuntime().addShutdownHook(new Thread(this::closeAllDrivers));
    }

    public WebDriver getDriver(String name, Browser browserType) {
        Optional<WebDriver> webDriverOptional = Optional.ofNullable(webDriverMap.get(name));
        if (webDriverOptional.isPresent()) {
            try {
                return webDriverOptional.get();
            } catch (NoSuchSessionException e){
                webDriverMap.remove(name);
                return initWebDriver(name, browserType);
            }
        } else {
            return initWebDriver(name, browserType);
        }
    }

    private WebDriver initWebDriver(String name, Browser browserType){
        logger().info("Instancing web driver [{}]", browserType);
        switch (browserType) {
            case CHROME:
                ChromeOptions chromeOptions = new ChromeOptions().merge(ChromeCapabilities.getChromeCapabilities());

                //Avoids issue: https://bugs.chromium.org/p/chromedriver/issues/detail?id=1901
                chromeOptions.addArguments("--start-maximized");

                WebDriver webDriver = new ChromeDriver(chromeOptions);
                webDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
                webDriverMap.putIfAbsent(name, webDriver);
            default:
                break;
        }

        return webDriverMap.get(name);
    }

    public WebDriver getDriver(String name) {
        Optional<WebDriver> webDriverOptional = Optional.ofNullable(webDriverMap.get(name));
        return webDriverOptional.orElse(null);
    }

    private void closeDriver(String name) {
        Optional<WebDriver> webDriverOptional = Optional.ofNullable(webDriverMap.get(name));
        if (webDriverOptional.isPresent()) {
            WebDriver webDriver = webDriverOptional.get();
            if (!webDriver.toString().contains("(null)")) {
                try {
                    webDriver.quit();
                } catch (Exception ignore){}
                webDriverMap.remove(name);
            }
        }
    }

    private void closeAllDrivers() {
        logger().info("Closing all WebDrivers");
        webDriverMap.forEach((name, webDriver) -> closeDriver(name));
    }

    public enum Browser {
        CHROME
    }
}
