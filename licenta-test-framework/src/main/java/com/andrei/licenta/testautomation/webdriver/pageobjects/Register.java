package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.model.UserModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Register extends PageObjectBase implements LogProvider {

    @FindBy(css = "#email")
    private WebElement emailInputField;

    @FindBy(css = "#username")
    private WebElement usernameInputField;

    @FindBy(css = "#password")
    private WebElement passwordInputField;

    @FindBy(css = "#matchingPassword")
    private WebElement passwordMatchingInputField;

    @FindBy(css = "#registerButton")
    private WebElement registerButton;

    @FindBy(xpath = "//*[@id=\"errorMessageEmail\"]")
    private WebElement errorMessageEmail;

    @FindBy(xpath = "//*[@id=\"errorMessageUsername\"]")
    private WebElement errorMessageUsername;

    @FindBy(xpath = "//*[@id=\"errorMessagePassword\"]")
    private WebElement errorMessagePassword;

    public Register(WebDriver webDriver) {
        super(webDriver);
    }

    public String getRegisterEmailErrorMessage() {
        logger().info("Getting register email error message");
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(errorMessageEmail));
        return errorMessageEmail.getText();
    }

    public String getRegisterUsernameErrorMessage() {
        logger().info("Getting register username error message");
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(errorMessageUsername));
        return errorMessageUsername.getText();
    }

    public String getRegisterPasswordErrorMessage() {
        logger().info("Getting register password error message");
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(errorMessagePassword));
        return errorMessagePassword.getText();
    }

    public void doRegister(String email, String user, String password, String matchingPassword){
        logger().info("Registering new user [{}, {}, {}, {}]", email, user, password, matchingPassword);

        emailInputField.clear();
        emailInputField.sendKeys(email);

        usernameInputField.clear();
        usernameInputField.sendKeys(user);

        passwordInputField.clear();
        passwordInputField.sendKeys(password);

        passwordMatchingInputField.clear();
        passwordMatchingInputField.sendKeys(matchingPassword);

        registerButton.click();
    }

    public void doRegister(UserModel user) {
        logger().info("Registering new user [{}, {}, {}, {}]", user.getEmail(), user.getUsername(), user.getPassword(), user.getMatchingPassword());

        emailInputField.clear();
        emailInputField.sendKeys(user.getEmail());

        usernameInputField.clear();
        usernameInputField.sendKeys(user.getUsername());

        passwordInputField.clear();
        passwordInputField.sendKeys(user.getPassword());

        passwordMatchingInputField.clear();
        passwordMatchingInputField.sendKeys(user.getMatchingPassword());

        registerButton.click();
    }

    public boolean isEmailValidationTriggered() {
        logger().info("Check if HTML5 login validation has triggered");
        return emailInputField.getAttribute("class").contains("invalid");
    }
}
