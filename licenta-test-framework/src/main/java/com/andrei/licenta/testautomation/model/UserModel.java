package com.andrei.licenta.testautomation.model;

import com.andrei.licenta.testautomation.utils.RandomGenerator;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserModel {

    @JsonDeserialize
    private String id;
    private String username;
    private String email;
    private String password;
    private String matchingPassword;

    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private String description;

    public UserModel() {
    }

    public static UserModel generateRandomUser(){
        UserModel userModel = new UserModel();
        String id = RandomGenerator.generateRandomId();
        String username = "TestUser" + id;
        String email = "TestUser" + id + "@licenta.fmi";
        String password = "UserToTest1!";

        userModel.setId(id);
        userModel.setUsername(username);
        userModel.setEmail(email);
        userModel.setPassword(password);
        userModel.setMatchingPassword(password);

        return userModel;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMatchingPassword() {
        return matchingPassword;
    }

    public void setMatchingPassword(String matchingPassword) {
        this.matchingPassword = matchingPassword;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", matchingPassword='" + matchingPassword + '\'' +
                '}';
    }
}
