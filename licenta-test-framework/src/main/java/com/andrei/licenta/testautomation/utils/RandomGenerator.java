package com.andrei.licenta.testautomation.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class RandomGenerator {

    private static SecureRandom random = new SecureRandom();

    public static String generateRandomId(){
        return new BigInteger(64, random).toString(10).substring(0, 9);
    }

}
