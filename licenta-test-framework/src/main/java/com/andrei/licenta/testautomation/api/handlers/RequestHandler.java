package com.andrei.licenta.testautomation.api.handlers;

import com.andrei.licenta.testautomation.api.APIConnection;
import com.andrei.licenta.testautomation.model.AuthenticationModel;
import com.andrei.licenta.testautomation.model.GenericResponse;
import com.andrei.licenta.testautomation.utils.JSONUtils;
import com.andrei.licenta.testautomation.utils.LogProvider;
import com.fasterxml.jackson.core.JsonProcessingException;

public abstract class RequestHandler implements LogProvider {

    private APIConnection apiConnection;
    private AuthenticationModel authenticationModel;

    RequestHandler(APIConnection apiConnection) {
        this.apiConnection = apiConnection;
    }

    public void setAuthenticationModel(AuthenticationModel authenticationModel) {
        this.authenticationModel = authenticationModel;
    }

    APIConnection getApiConnection() {
        if (authenticationModel != null) {
            apiConnection.addHeader("username", authenticationModel.getEmail());
            apiConnection.addHeader("password", authenticationModel.getPassword());
        }
        return apiConnection;
    }

    AuthenticationModel getAuthenticationModel() {
        return authenticationModel;
    }

    public GenericResponse getApiConnectionResponse() {
        return getApiConnection().getLatestResponse().getBody().as(GenericResponse.class);
    }

    String getJson(Object obj) throws JsonProcessingException {
        return JSONUtils.getJsonMapper().writerWithDefaultPrettyPrinter().writeValueAsString(obj);
    }
}
