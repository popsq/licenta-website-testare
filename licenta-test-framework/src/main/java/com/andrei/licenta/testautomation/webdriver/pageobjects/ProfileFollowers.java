package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.model.FollowerModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.ArrayList;
import java.util.List;

public class ProfileFollowers extends PageObjectBase implements LogProvider {

    @FindBy(xpath = "//*[@id=\"followersTitle\"]//span")
    private WebElement followersHeader;
    @FindBy(xpath = "//*[@id=\"noFollowers\"]//span")
    private WebElement noFollowersLabel;
    @FindBy(xpath = "//ul[@id=\"followers\"]/li")
    private List<WebElement> followerElements;

    private String followerLocator = "//form[starts-with(@id,'follow')]";
    private String followerUsernameLocator = "//span[@id='followUsername-%s']";
    private String followerFollowDateLocator = "//span[@id='followDate-%s']";
    private String followerFollowButtonLocator = "//button[@id='followButton-%s']";

    public ProfileFollowers(WebDriver webDriver) {
        super(webDriver);
    }

    public List<FollowerModel> getAllFollowers(){
        logger().info("Getting all followers");
        List<FollowerModel> followerModelList = new ArrayList<>();

        followerElements.forEach(webElement -> {
            FollowerModel followerModel = new FollowerModel();

            WebElement followerForm = webElement.findElement(By.xpath(followerLocator));
            String followerId = followerForm.getAttribute("id").replace("follow-","");

            followerModel.setId(followerId);
            followerModel.setUsername(followerForm.findElement(By.xpath(String.format(followerUsernameLocator, followerId))).getText());
            followerModel.setFollowDate(followerForm.findElement(By.xpath(String.format(followerFollowDateLocator, followerId))).getText());
            followerModel.setFollowed(isFollowed(followerId));

            followerModelList.add(followerModel);
        });

        return followerModelList;
    }

    public boolean noFollowersPresent(){
        try {
            return noFollowersLabel.getText().equalsIgnoreCase("Oops, you don't seem to have any followers.") ||
                    noFollowersLabel.getText().equalsIgnoreCase("This user does not have any followers currently.");
        } catch (NoSuchElementException e){
            return false;
        }
    }

    public boolean isFollowed(String id) {
        try {
            return getWebDriver().findElement(By.xpath(String.format(followerFollowButtonLocator + "//i", id))).getText().trim().equalsIgnoreCase("favorite");
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private void changeFollowStatusUser(String id, boolean isLike){
        if (isLike) logger().info("Following user with id {}", id);
        else logger().info("Unfollowing user with id {}", id);
        getWebDriver().findElement(By.xpath(String.format(followerFollowButtonLocator, id))).click();
    }

    public void followUser(String id) {
        changeFollowStatusUser(id, true);
    }

    public void unfollowUser(String id) {
        changeFollowStatusUser(id, false);
    }

}
