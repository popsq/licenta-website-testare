package com.andrei.licenta.testautomation.api;

public interface APIPaths {

    //User API Paths
    String userPath = "/api/user";

    String registerUser = userPath + "/add";
    String userName = userPath + "/%s";
    String followers = userPath + "/%s/followers";
    String followUser = userPath + "/%s/follow";
    String unfollowUser = userPath + "/%s/unfollow";
    String searchUser = userPath + "/search";

    //Tweet API Paths
    String tweetPath = "/api/tweet";

    String tweetUserName = tweetPath + "/%s";
    String likeTweet = tweetPath + "/%s/like";
    String unlikeTweet = tweetPath + "/%s/unlike";
    String searchTweet =  tweetPath + "/search";
    String cleanup = userPath + "/testcleanup";
}
