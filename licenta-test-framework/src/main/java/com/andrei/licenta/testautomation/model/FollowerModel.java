package com.andrei.licenta.testautomation.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FollowerModel {

    private String id;
    private String username;
    private String followDate;
    private boolean isFollowed;

    //API
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private UserModel user;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private UserModel userFollowing;
    @JsonInclude(value = JsonInclude.Include.NON_NULL)
    private boolean active;

    public FollowerModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFollowDate() {
        return followDate;
    }

    public void setFollowDate(String followDate) {
        this.followDate = followDate;
    }

    public boolean isFollowed() {
        return isFollowed;
    }

    public void setFollowed(boolean followed) {
        isFollowed = followed;
    }

    public UserModel getUser() {
        return user;
    }

    public void setUser(UserModel user) {
        this.user = user;
    }

    public UserModel getUserFollowing() {
        return userFollowing;
    }

    public void setUserFollowing(UserModel userFollowing) {
        this.userFollowing = userFollowing;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }
}
