package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Header extends PageObjectBase implements LogProvider {

    @FindBy(css = "#logoutButton")
    private WebElement logoutButton;

    @FindBy(xpath = "//*[@id=\"search\"]")
    private WebElement searchBar;

    @FindBy(xpath = "/html/body/header/div/nav/div/div/div/div[3]/div/div[1]/b")
    private WebElement headerUser;

    public Header(WebDriver webDriver) {
        super(webDriver);
    }

    public void doSearch(String searchValue){
        logger().info("Searching tweets and users containing text [{}]", searchValue);

        try {
            searchBar.clear();
            searchBar.sendKeys(searchValue);
            searchBar.submit();
        } catch (NoSuchElementException e){
            logger().info("No search bar found. Are you logged out?");
        }
    }

    public String getUserText(){
        try {
            logger().info("Getting header user");
            return headerUser.getText();
        } catch (NoSuchElementException e){
            logger().info("No username found in header. Are you logged out?");
            return "";
        }
    }

    public void logout() {
        try {
            logoutButton.click();
        } catch (NoSuchElementException ex) {
            logger().info("User already logged out");
        }
    }
}
