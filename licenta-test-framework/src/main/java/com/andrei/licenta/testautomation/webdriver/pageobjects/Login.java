package com.andrei.licenta.testautomation.webdriver.pageobjects;

import com.andrei.licenta.testautomation.model.UserModel;
import com.andrei.licenta.testautomation.utils.LogProvider;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * MIT License
 * <p>
 * Copyright (c) 2017 Andrei Popescu
 * <p>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
public class Login extends PageObjectBase implements LogProvider {

    @FindBy(how = How.ID, using = "email")
    private WebElement emailInputField;

    @FindBy(id = "password")
    private WebElement passwordInputField;

    @FindBy(id = "loginButton")
    private WebElement loginButton;

    @FindBy(id = "registerButton")
    private WebElement registerButton;

    @FindBy(xpath = "//*[@id=\"errorMessageLogin\"]")
    private WebElement loginError;

    public Login(WebDriver webDriver) {
        super(webDriver);
    }

    public void doLogin(String email, String password) {
        logger().info("Logging into website with email [{}] and password [{}]", email, password);
        emailInputField.clear();
        emailInputField.sendKeys(email);
        passwordInputField.clear();
        passwordInputField.sendKeys(password);
        loginButton.click();
    }

    public void doLogin(UserModel user){
        doLogin(user.getEmail(), user.getPassword());
    }

    public void goToRegisterPage() {
        logger().info("Going to register page");
        registerButton.click();
    }

    public String getLoginErrorMessage() {
        logger().info("Getting login error message");
        WebDriverWait webDriverWait = new WebDriverWait(getWebDriver(), 10);
        webDriverWait.until(ExpectedConditions.visibilityOf(loginError));
        return loginError.getText();
    }

    public boolean isValidationTriggered() {
        logger().info("Check if HTML5 login validation has triggered");
        return emailInputField.getAttribute("class").contains("invalid");
    }
}
